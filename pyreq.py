import argparse
import subprocess
import sys
import os
import inspect
import tempfile
from typing import Union, List

try:
    from pip._internal.utils.misc import get_installed_distributions
    from pip._internal.operations.freeze import FrozenRequirement
except ImportError:
    from pip import get_installed_distributions, FrozenRequirement

__version__ = '1.0.0'


def handle_different_python(interpreter: str) -> Union[None, int]:
    interpreter_path = os.path.abspath(interpreter)

    # if target is not current python, run the script under the specified interpreter
    if interpreter_path != os.path.abspath(sys.executable):
        argv = sys.argv[1:] # cause sys.argv[0] = python executable

        for i, value in enumerate(argv):
            if value == '--python':
                del argv[i]
                del argv[i]  # because --python <interpreter>
            elif value.startswith('--python'):
                del argv[i]

        # get the path to the current script
        script_path = inspect.getsourcefile(sys.modules[__name__])
        with open(script_path, 'rt') as f:
            data = f.read()
        cmd = [interpreter_path, '-c', data, *argv]

        tmp = tempfile.mkdtemp()
        try:
            return subprocess.call(cmd, cwd=tmp)
        finally:
            os.removedirs(tmp)

    return None


def get_installed_pkg(local_only: bool = True, user_only: bool = False, frozen: bool = False) -> List[str]:
    pkg = get_installed_distributions(local_only=local_only, user_only=user_only)
    lines = [''] * len(pkg)
    if frozen:
        for i in range(len(pkg)):
            try:
                req = FrozenRequirement.from_dist(pkg[i])
            except TypeError:
                req = FrozenRequirement.from_dist(pkg[i], [])

            lines[i] = str(req).strip()
    else:
        lines = [f'{p.project_name}=={p.version}' for p in pkg]

    return lines


def main() -> int:
    # argument parser
    parser = argparse.ArgumentParser(description='A tool to write a requirements.txt')
    parser.add_argument('-f', '--freeze', action='store_true', help='Print names to write freeze files')
    parser.add_argument('--python', default=sys.executable, help='Python executable')
    parser.add_argument('-l', '--local-only', action='store_true', help='Do not show globally installed packages in '
                                                                        'an venv')
    parser.add_argument('-u', '--user-only', action='store_true', help='Only show installed packages in user site dir')
    args = parser.parse_args()

    result = handle_different_python(args.python)
    if result is not None:
        return result

    # get installed packages
    lines = get_installed_pkg(args.local_only, args.user_only, args.freeze)

    print('\n'.join(lines))

    return 0


if __name__ == '__main__':
    sys.exit(main())
