# Pyreq

Pyreq is a single script that allows you to automatically write a requirements.txt

<!-- TABLE OF CONTENTS -->
## Table of contents
1. [Getting started](#getting-started)
    1. [Prerequisites](#prerequisites)
    2. [Installation](#installation)
    
2. [Usage](#usage)
3. [Contributing](#contributing)
4. [License](#license)

## Getting started
### Prerequisites
This script is written with `typing` which is only supported since `Python 3.5`,
therefore make sure your python can run this script.

To know the version of your python write:
```bash
$ python --version # or python3, it depends on your system
Python 3.9.1
```

### Installation
For the moment `pyreq` is not supported with [pip](https://pip.pypa.io/en/stable/).

1. Clone the repository
```bash
$ git clone https://gitlab.com/Zarato/pyreq.git
```
or you can directly download it with `wget`
```bash
$ wget -O pyreq.py https://gitlab.com/Zarato/pyreq/-/raw/master/pyreq.py?inline=false
```

2. Make the script executable
```bash
$ chmod +x pyreq.py
```

## Usage

```
usage: pyreq.py [-h] [-f] [--python PYTHON] [-l] [-u]

A tool to write a requirements.txt

optional arguments:
  -h, --help        show this help message and exit
  -f, --freeze      Print names to write freeze files
  --python PYTHON   Python executable
  -l, --local-only  Do not show globally installed packages in an venv
  -u, --user-only   Only show installed packages in user site dir
```

To write a `requirements.txt` you simply have to execute the script.

Here is an example where I installed `numpy`.
```bash
$ pip install numpy
...
$ python pyreq.py > requirements.txt
$ cat requirements.txt
setuptools==53.0.0
pip==21.0.1
numpy==1.20.0
```

## Contributing

Pull requests are welcome.

Please open an issue if you would like to make some changes.

## License
[MIT](https://choosealicense.com/licenses/mit/)